import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

createApp(App).use(router).mount('#app')

// new Vue({
//     el: '#app',
//     components: {App},
//     template: '<App/>'
// })