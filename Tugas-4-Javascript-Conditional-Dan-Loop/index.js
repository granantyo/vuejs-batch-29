// soal 1
var nilai;
nilai = 77;

if (nilai >= 85) {
console.log("indeksnya A");    
} else {
    if (nilai >= 75 && nilai < 85) {
        console.log("indeksnya B");
    } else {
        if (nilai >= 65 && nilai < 75) {
            console.log("indeksnya C");
        } else {
            if (nilai >= 55 && nilai < 65) {
                console.log("indeksnya D");
            } else {
                console.log("indeksnya E");
            }
        }
    }
}

// soal 2
var tanggal = 22;
var bulan = 7;
var tahun = 1992;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;        
    default:
        break;
}
console.log(tanggal+" "+bulan+" "+tahun);

//soal 3
// var m = 3;
// var i = 0;
// while (i<=m) {
//     n=i;
//     for (let index = 0; index < n; index++) {
//         process.stdout.write("#");
//     }
//     console.log("");
//     i++;
// }

//soal 4
var m=3;
while (m=3) {
    console.log("1 - I love programming");
    console.log("2 - I love Javascript");
    console.log("3 - I love VueJS");
}