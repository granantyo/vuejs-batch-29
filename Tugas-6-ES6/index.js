//soal1
const luaspp = (pjg,lbr) => {
    return pjg * lbr;
} 

const kelilingpp = (pjg,lbr) => {
    return (pjg + lbr) * 2;
}


//soal2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log(firstName + " " + lastName)
        } 
    }
}

  //Driver Code 
newFunction("William", "Imoh").fullName()


//soal3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby)


//soal4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

let combined = [...west,...east];

//Driver Code
console.log(combined)


//soal5
const planet = "earth" 
const view = "glass" 
//var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const after = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`
console.log(after);